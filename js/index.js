$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();  
    $('.carousel').carousel({
      interval: 2000
    });    
    
       $("#exampleModal").on('show.bs.modal', function (e) {
       console.log('modal mostrandose');
       $("#comentarios").removeClass('btn-outline-success');
       $("#comentarios").addClass('btn-primary');
       $("#comentarios").prop('disabled',true);
      });
      $("#exampleModal").on('shown.bs.modal', function (e) {
       console.log('modal mostrado');          
      });      

      $("#exampleModal").on('hide.bs.modal', function (e) {
       console.log('modal ocultandose');
       $("#comentarios").addClass('btn-outline-success');
       $("#comentarios").removeClass('btn-primary');
       $("#comentarios").prop('disabled',false);
      });
      $("#exampleModal").on('hidden.bs.modal', function (e) {
       console.log('modal ocultado');
      });      

});